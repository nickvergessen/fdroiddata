Categories:System,Time
License:GPLv3
Web Site:
Source Code:https://github.com/quarck/CalendarNotification
Issue Tracker:https://github.com/quarck/CalendarNotification/issues

Auto Name:Calendar Notifications Plus
Summary:Advanced calendar notifications
Description:
Replace calendar notifications with advanced notifications, allowing custom
snooze times and reboot notifications persistence. The app is focusing on making
its functionality as transparent as possible, notification would behave in the
same way as stock calendar notifications. All calendar applications should be
handled correctly in theory.
.

Repo Type:git
Repo:https://github.com/quarck/CalendarNotification

Build:1.0.1,2
    commit=ed8f05c87f90e982885e9198999edb7fbe92612e
    subdir=app
    gradle=yes

Build:1.0.3,5
    commit=Release_1.0.3
    subdir=app
    gradle=yes

Build:1.0.9,10
    commit=Release_1.0.9
    subdir=app
    gradle=yes

Build:1.0.10,11
    commit=Release_1.0.10
    subdir=app
    gradle=yes

Build:1.0.11,12
    commit=Release_1.0.11
    subdir=app
    gradle=yes

Build:1.0.12,13
    commit=Release_1.0.12
    subdir=app
    gradle=yes

Build:1.0.16,16
    commit=Release_1.0.16
    subdir=app
    gradle=yes

Build:1.0.17,17
    commit=Release_1.0.17
    subdir=app
    gradle=yes

Build:1.0.18,18
    commit=Release_1.0.18
    subdir=app
    gradle=yes

Build:1.0.19,19
    commit=Release_1.0.19
    subdir=app
    gradle=yes

Auto Update Mode:Version Release_%v
Update Check Mode:Tags ^Release
Current Version:1.0.19
Current Version Code:19
